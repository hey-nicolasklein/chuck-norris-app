import React from 'react'
import style from './JokeCard.module.css'


const JokeCard = React.memo(({joke, categories, createdAt, icon, obj}) => {
    return(
        <div className={style.jokeCard}>
            <h3>{obj.value}</h3>
            <p>{obj.created_at}</p>
            <img src={obj.icon_url}/>
        </div>
    )
});

export default JokeCard;