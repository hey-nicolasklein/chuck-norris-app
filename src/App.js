import React,{useEffect, useState, useCallback} from 'react';
import logo from './logo.svg';
import './App.css';
import Recipe from './JokeCard';
import JokeCard from './JokeCard';

const App = () => {

  const RANDOM_JOKE = "https://api.chucknorris.io/jokes/random";

  const [collection, setCollection] = useState([]);
  const [filteredCollection, setFilteredCollection] = useState([]);

  const [search, setSearch] = useState("");
  const [query, setQuery] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  const [error, setError] = useState(false);


  /*
  Runs when the app mounts
  */ 
  useEffect(()=>{
    getJokes();
  },[]);

  /*
  Runs when the query state changes
  */
  useEffect(() =>{
    filterJokes();
  },[query]);

  /* 
  Filters jokes depending on the query state
  */
  const filterJokes = useCallback(() => {
    console.log(`filter by value: "${query}"`);
    setFilteredCollection(collection.filter(e => e.value.includes(query)));
    console.table(filteredCollection);
  });

  /* 
  Fetches jokes from the chucknorris.io api
  */
  const getJokes = useCallback(async () => {
    let i=0;
    let results = [];

    setIsLoading(true);
    while(i<6){
      try{
        const response = await fetch(RANDOM_JOKE);
        const data = await response.json();
        results.push(data);
        i++; 
      }
      catch{
        setError(true);
      }
    }
    setCollection(results);
    setFilteredCollection(results);
    setIsLoading(false);
  });

  /*
  Updates the search variable,
  which holds incomplete querys

  TODO change to instantly filter,
  because no external api is used here.
  */
  const updateSearch = e => {
    setSearch(e.target.value);
  }

  /*
  Is called when the search button
  gets clicked. 
  The user input is finished and results
  can be displayed.
  */
  const getSearch = e =>{
    e.preventDefault();
    setQuery(search);
    setSearch("");
  }

  const delete_one = () => {
    let new_col = [...filteredCollection];
    new_col = new_col.filter((val, index) => {
      return index != 0
    });
    setFilteredCollection(new_col);
  };

  return(
    <div className="App">
      <h1>Chuck Jokes</h1>
      <form onSubmit={getSearch} className="search-form">
        <input className="search-bar" type="text" value={search}
                onChange={updateSearch}/>
        <button className="search-button" type="submit">
          Search
        </button>
        <button onClick={delete_one}> Delete Top!</button>

      </form>
      {isLoading ? (<div> is loading</div>) : (
      <div className="Container">
        {filteredCollection.map(e => (<JokeCard obj={e} key={e.id}/>))}
      </div>
      )}
    </div>
  )
}

export default App;
